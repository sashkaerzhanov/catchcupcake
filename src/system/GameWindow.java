package system;

/**
 * @author Alexander Erzhanov
 * @since 15.09.2017
 */

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;


public class GameWindow extends JFrame {

    private static GameWindow game_window;
    private static long last_frame_time;
    private static Image background;
    private static Image gameover;
    private static Image cupcake;
    private static float cake_left = 200;
    private static float cake_top = -100;
    private static float cake_v = 200;
    private static int score;
    public static void main(String[] args) throws IOException {
        background = ImageIO.read(GameWindow.class.getResourceAsStream("background.jpg"));
        gameover = ImageIO.read(GameWindow.class.getResourceAsStream("game_over.png"));
        cupcake = ImageIO.read(GameWindow.class.getResourceAsStream("cupcake.png"));
        game_window = new GameWindow();
        game_window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        game_window.setLocation(100,100);
        game_window.setSize(1100,600);
        game_window.setResizable(false);
        last_frame_time = System.nanoTime();
        GameField game_field = new GameField();
        game_field.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                float drop_right = cake_left + cupcake.getWidth(null);
                float drop_bottom =cake_top + cupcake.getHeight(null);
                boolean is_cake = x >=cake_left && x <= drop_right && y >= cake_top && y <= drop_bottom;
                if (is_cake){
                    cake_top = -100;
                    cake_left = (int) (Math.random() *(game_field.getWidth()-cupcake.getWidth(null)) );
                    cake_v = cake_v +20;
                    score ++;
                    game_window.setTitle("Score:  " + score);
                }
            }
        });
        game_window.add(game_field);
        game_window.setVisible(true);
    }
    private static void onRepaint(Graphics g) {
        long current_time = System.nanoTime();
        float delta_time = (current_time - last_frame_time) * 0.000000001f;
        last_frame_time = current_time;
        cake_top = cake_top + cake_v * delta_time;


        g.drawImage(background,0,0,null);
        g.drawImage(cupcake,(int) cake_left,(int) cake_top,null);
        if (cake_top > game_window.getHeight()) g.drawImage(gameover, 400,200,null);
    }
    private static class GameField extends JPanel {

        @Override
        protected void paintComponent(Graphics g){
            super.paintComponent(g);
            onRepaint(g);
            repaint();
        }
    }
}
